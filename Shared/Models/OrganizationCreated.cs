namespace Shared.Models;

/// <summary>
/// Сообщение о создании организации. . ExchangeType Fanout 
/// </summary>
public class OrganizationCreated
{
    /// <summary>
    /// Название организации
    /// </summary>
    public required string Name { get; init; }
}