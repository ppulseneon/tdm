using System.Text.RegularExpressions;
using UserService.Domain.Abstractions;
using UserService.Domain.OrganizationAggregate;
using UserService.Domain.UserAggregate.Exceptions;

namespace UserService.Domain.UserAggregate;

public class User : AggregateRoot
{
    /// <summary>
    /// Регулярный выражение для проверки email
    /// </summary>
    public static Regex RegexEmail => new Regex(@"^[^@\s]+@[^@\s]+\.[^@\s]+$");

    /// <summary>
    /// Регулярный выражение для проверки email
    /// </summary>
    public static Regex RegexPhoneNumber => new Regex(@"^\+\d{11}$");

    /// <summary>
    /// Максимальное количество символов в имени
    /// </summary>
    public const int MaxFirstNameLength = 50;

    /// <summary>
    /// Минимальное количество символов в имени
    /// </summary>
    public const int MinFirstNameLength = 3;

    /// <summary>
    /// Максимальное количество символов в фамилии
    /// </summary>
    public const int MaxLastNameLength = 50;

    /// <summary>
    /// Минимальное количество символов в фамилии
    /// </summary>
    public const int MinLastNameLength = 3;

    /// <summary>
    /// Максимальное количество символов в отчестве
    /// </summary>
    public const int MaxPatronymicLength = 50;

    /// <summary>
    /// Минимальное количество символов в отчестве
    /// </summary>
    public const int MinPatronymicLength = 3;

    /// <summary>
    /// Инициализация пользователя
    /// </summary>
    /// <param name="id">Идентификатор пользователя</param>
    private User(Guid id) : base(id)
    {
        FirstName = null!;
        LastName = null!;
        Email = null!;
        PhoneNumber = null!;
    }

    /// <summary>
    /// Имя
    /// </summary>
    public string FirstName { get; private init; }

    /// <summary>
    /// Фамилия
    /// </summary>
    public string LastName { get; private init; }

    /// <summary>
    /// Отчество
    /// </summary>
    public string? Patronymic { get; private init; }

    /// <summary>
    /// Номер телефона
    /// </summary>
    public string PhoneNumber { get; private init; }

    /// <summary>
    /// Электронная почта
    /// </summary>
    public string Email { get; private set; }

    /// <summary>
    /// Идентификатор организации
    /// </summary>
    public Guid? OrganizationId { get; private set; }

    public void SetToOrganization(Organization organization)
    {
        //Проверка состоит ли пользователь в организации
        if (this.OrganizationId.HasValue)
            //Если пользователь состоит в организации выбрасываем исключение
            throw new UserAlreadyInOrganizationException(Id, OrganizationId.Value);

        //Устанавливаем пользователя в организацию
        OrganizationId = organization.Id;
    }

    /// <summary>
    /// Метод для создания агрегата пользователя
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <param name="firstName">Имя</param>
    /// <param name="lastName">Фамилия</param>
    /// <param name="patronymic">Отчество</param>
    /// <param name="email">Электронная почта</param>
    /// <param name="phoneNumber">Номер телефона</param>
    /// <param name="organizationId">Идентификатор организации</param>
    /// <returns>Агрегат пользователя</returns>
    /// <exception cref="IncorrectFirstNameException">Исключение вызывается при некорректном имени</exception>
    /// <exception cref="IncorrectLastNameException">Исключение вызывается при некорректной фамилии</exception>
    /// <exception cref="IncorrectPatronymicException">Исключение вызывается при некорректном отчестве</exception>
    /// <exception cref="IncorrectEmailException">Исключение вызывается при некорректной почте</exception>
    /// <exception cref="IncorrectPhoneNumberException">Исключение вызывается при некорректном номере телефона</exception>
    public static User Create(
        Guid id,
        string firstName,
        string lastName,
        string? patronymic,
        string email,
        string phoneNumber,
        Guid? organizationId = null)
    {
        if (firstName.Length is < MinFirstNameLength or > MaxFirstNameLength)
            //Если некорректное имя выбрасываем исключение
            throw new IncorrectFirstNameException(MinFirstNameLength, MaxFirstNameLength);

        //Проверка корректности фамилии
        if (lastName.Length is < MinLastNameLength or > MaxLastNameLength)
            //Если некорректная фамилия выбрасываем исключение
            throw new IncorrectLastNameException(MinLastNameLength, MaxLastNameLength);

        //Проверка корректности отчества
        if (!string.IsNullOrEmpty(patronymic) && patronymic.Length is < MinPatronymicLength or > MaxPatronymicLength)
            //Если некорректное отчество выбрасываем исключение
            throw new IncorrectPatronymicException(MinPatronymicLength, MaxPatronymicLength);

        //Проверка корректности почты
        if (!RegexEmail.IsMatch(email))
            //Если некорректная почта выбрасываем исключение
            throw new IncorrectEmailException();

        //Проверка корректности номера телефона
        if (!RegexPhoneNumber.IsMatch(phoneNumber))
            //Если некорректное имя выбрасываем исключение
            throw new IncorrectPhoneNumberException();

        //Возвращаем аггрегат пользователя
        return new User(id)
        {
            OrganizationId = organizationId,
            FirstName = firstName,
            LastName = lastName,
            Patronymic = patronymic,
            Email = email,
            PhoneNumber = phoneNumber
        };
    }
}