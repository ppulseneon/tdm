using UserService.Domain.Abstractions;
using UserService.Domain.UserAggregate;
using UserService.Domain.OrganizationAggregate.Exceptions;

namespace UserService.Domain.OrganizationAggregate;

/// <summary>
/// Аггрегат организации
/// </summary>
public class Organization : AggregateRoot
{
    /// <summary>
    /// Максимальное количество символов в имени
    /// </summary>
    public const int MaxNameLength = 100;

    /// <summary>
    /// Минимальное количество символов в имени
    /// </summary>
    public const int MinNameLength = 3;

    /// <summary>
    /// Инициализирует новый экземпляр аггрегата
    /// </summary>
    /// <param name="id">Идентификатор</param>
    private Organization(Guid id) : base(id)
    {
        Name = null!;
    }

    /// <summary>
    /// Метод для создания Организации
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <param name="name">Название организации</param>
    /// <returns>Аггрегат организации</returns>
    /// <exception cref="IncorrectOrgNameException">Исключение вызывается при некорректном имени</exception>
    public static Organization Create(Guid id, string name)
    {
        //Проверка корректности названия организации 
        if (name.Length < MinNameLength || name.Length > MaxNameLength)
            //Если некорректное имя выбрасываем исключение
            throw new IncorrectOrgNameException(MinNameLength, MaxNameLength);
        
        //Возвращаем аггрегат
        return new Organization(id)
        {
            Name = name
        };
    }

    /// <summary>
    /// Название организации
    /// </summary>
    public string Name { get; private init; }
}