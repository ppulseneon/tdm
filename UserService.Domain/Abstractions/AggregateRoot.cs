namespace UserService.Domain.Abstractions;

/// <summary>
/// Корень агрегата
/// </summary>
/// <param name="id">Иднетификатор аггрегата</param>
public abstract class AggregateRoot(Guid id)
{
    /// <summary>
    /// Идентификатор
    /// </summary>
    public Guid Id => id;
}