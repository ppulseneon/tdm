using Moq;
using UserService.Application.Abstractions.Commands;
using UserService.Application.Services.CommandsHandlers;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.UserAggregate;

namespace UserServices.Tests;

public class AddUserCommandHandlerTests
{
    private Mock<IUserRepository> _userStoreMock = null!;
    private AddUserCommandHandler _userCommandHandler = null!;

    [SetUp]
    public void SetUp()
    {
        _userStoreMock = new Mock<IUserRepository>();
        _userCommandHandler = new AddUserCommandHandler(_userStoreMock.Object);
    }

    [TestCase("Максим", "Завгородний", "+79586745866", "zvg@yandex.com", "Валерьевич")]
    [TestCase("Максим", "Завгородний", "+79586745866", "zvg@yandex.com", null)]
    public async Task Handle_ValidUser_AddsUserToStore(string firstName, string lastName, string phone, string email,
        string patronymic)
    {
        // Arrange
        var command = new AddUserCommand
        {
            FirstName = firstName,
            LastName = lastName,
            Patronymic = patronymic,
            Email = email,
            PhoneNumber = phone
        };

        // Act
        await _userCommandHandler.Handle(command, CancellationToken.None);

        // Assert
        _userStoreMock.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Once);
    }

}