using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using UserService.Application.Abstractions.Commands;
using UserService.Application.Abstractions.Queries;
using UserService.Domain.UserAggregate;
using UserService.Presentation.Contracts.Organization;
using UserService.Presentation.Contracts.User;
using UserService.Presentation.Contracts.User.Validators;
using UserService.Presentation.Controllers;

namespace UserServices.Tests
{
    [TestFixture]
    public class UserControllerTests
    {
        private UserController _controller = null!;
        private Mock<IMediator> _mediatorMock = null!;

        [SetUp]
        public void SetUp()
        {
            _mediatorMock = new Mock<IMediator>();
            var validator = new AddUserToOrganizationInputModelValidator();

            _controller = new UserController(_mediatorMock.Object, validator);
        }

        [Test]
        public async Task GetUsers_ReturnsUsers()
        {
            // Arrange
            var organizationId = Guid.NewGuid();
            var users = new List<User>
            {
                User.Create(Guid.NewGuid(), 
                    "Максим", 
                    "Завгородний", 
                    "Валерьевич", 
                    "zvg@yandex.com",
                "+79586745866"),
                User.Create(Guid.NewGuid(), 
                    "Николай", 
                    "Загородный", 
                    null, 
                    "zfgfgg@yandex.ru",
                    "+79345645856"),
                
            };

            _mediatorMock.Setup(x => x.Send(It.IsAny<GetUsersQuery>(), CancellationToken.None))
                .ReturnsAsync(users);

            // Act
            var result = await _controller.GetUsers(organizationId, 1, 15, CancellationToken.None) as OkObjectResult;

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(result?.Value, Is.Not.Null);
                Assert.That((result!.Value as UserViewModel[])!, Has.Length.EqualTo(2));
            });
        }

        [Test]
        public async Task AddToOrganization_WithValidModel_ReturnsOk()
        {
            // Arrange
            var model = new AddUserToOrganizationInputModel(
                OrganizationId: Guid.NewGuid(),
                Userid: Guid.NewGuid()
            );

            _mediatorMock.Setup(x => x.Send(It.IsAny<AddUserToOrganizationCommand>(), CancellationToken.None))
                .Returns(Task.CompletedTask);

            // Act
            var result = await _controller.AddToOrganization(model, CancellationToken.None) as OkResult;

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result!.StatusCode, Is.EqualTo(200));
        }

        [Test]
        public async Task AddToOrganization_WithInvalidModel_ReturnsBadRequest()
        {
            // Arrange
            var model = new AddUserToOrganizationInputModel(null, null);

            // Act
            var result = await _controller.AddToOrganization(model, CancellationToken.None) as BadRequestObjectResult;

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result!.StatusCode, Is.EqualTo(400));
        }
    }
}