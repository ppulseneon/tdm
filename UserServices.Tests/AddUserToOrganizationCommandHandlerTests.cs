using Microsoft.Extensions.Logging;
using Moq;
using UserService.Application.Abstractions.Commands;
using UserService.Application.Abstractions.Exceptions;
using UserService.Application.Services.CommandsHandlers;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.OrganizationAggregate;
using UserService.Domain.UserAggregate;
using UserService.Domain.UserAggregate.Exceptions;

namespace UserServices.Tests;

[TestFixture]
public class AddUserToOrganizationCommandHandlerTests
{
    private User _user = null!;
    private Organization _organization = null!;
    private Mock<ILogger<AddUserToOrganizationCommandHandler>> _loggerMock = null!;

    [SetUp]
    public void SetUp()
    {
        _loggerMock = new Mock<ILogger<AddUserToOrganizationCommandHandler>>();
        _user = User.Create(
            id: Guid.NewGuid(), 
            firstName: "Максим", 
            lastName: "Завгородний", 
            patronymic: "Валерьевич",
            phoneNumber: "+79396743685", 
            email: "abc@yandex.rom");
        _organization = Organization.Create(Guid.NewGuid(), "Тестовая организация");
    }

    [Test]
    public async Task Handle_ValidCommand_AddsUserToOrganization()
    {
        // Arrange
        var userStoreMock = new Mock<IUserRepository>();
        userStoreMock.Setup(x => x.GetAsync(_user.Id)).ReturnsAsync(_user);

        var organizationStoreMock = new Mock<IOrganizationRepository>();
        organizationStoreMock.Setup(x => x.GetAsync(_organization.Id)).ReturnsAsync(_organization);
        
        var request = new AddUserToOrganizationCommand
        {
            OrganizationId = _organization.Id,
            UserId = _user.Id
        };
        
        var handler =
            new AddUserToOrganizationCommandHandler(userStoreMock.Object, organizationStoreMock.Object,
                _loggerMock.Object);

        // Act
        await handler.Handle(request, CancellationToken.None);

        // Assert
        userStoreMock.Verify(x => x.UpdateAsync(_user), Times.Once);
        _loggerMock.VerifyLog(logger => logger.LogInformation(
            "Пользователь {LastName} {FirstName} добавлен в организацию {OrgName}", _user.LastName,
            _user.FirstName, _organization.Name));
    }

    [Test]
    public Task Handle_WhenUserAlreadyInOrganization_ThrowsUserAlreadyInOrganizationException()
    {
        // Arrange
        var user = User.Create(
            id: Guid.NewGuid(), 
            firstName: "Максим", 
            lastName: "Завгородний", 
            patronymic: "Валерьевич",
            phoneNumber: "+79396743685", 
            email: "abc@yandex.rom");
        
        var userOrganization = Organization.Create(Guid.NewGuid(),"Оргаинзация пользователя");
        user.SetToOrganization(userOrganization);

        var request = new AddUserToOrganizationCommand
        {
            OrganizationId = _organization.Id,
            UserId = _user.Id
        };

        var userStoreMock = new Mock<IUserRepository>();
        userStoreMock.Setup(x => x.GetAsync(request.UserId)).ReturnsAsync(user);

        var organizationStoreMock = new Mock<IOrganizationRepository>();
        organizationStoreMock.Setup(x => x.GetAsync(request.OrganizationId)).ReturnsAsync(_organization);
        organizationStoreMock.Setup(x => x.GetAsync(user.OrganizationId!.Value)).ReturnsAsync(userOrganization);

        var handler =
            new AddUserToOrganizationCommandHandler(userStoreMock.Object, organizationStoreMock.Object,
                _loggerMock.Object);

        // Act & Assert
        var exception =
            Assert.ThrowsAsync<UserAlreadyInOrganizationException>(() =>
                handler.Handle(request, CancellationToken.None));
        Assert.That(exception, Is.Not.Null);
        Assert.That(exception!.IdUser, Is.EqualTo(request.UserId));

        return Task.CompletedTask;
    }

    [Test]
    public Task Handle_WhenOrganizationNotFound_ThrowsOrganizationNotFoundException()
    {
        // Arrange
        var orgId = Guid.NewGuid();
        var request = new AddUserToOrganizationCommand
        {
            OrganizationId = _organization.Id,
            UserId = _user.Id
        };
        var userStore = new Mock<IUserRepository>();
        var organizationStore = new Mock<IOrganizationRepository>();

        userStore.Setup(x => x.GetAsync(request.UserId)).ReturnsAsync(_user);
        organizationStore.Setup(x => x.GetAsync(request.OrganizationId)).ReturnsAsync(() => null);

        var handler =
            new AddUserToOrganizationCommandHandler(userStore.Object, organizationStore.Object, _loggerMock.Object);

        // Act & Assert
        var exception = Assert.ThrowsAsync<OrganizationNotFoundException>(() =>
            handler.Handle(request, CancellationToken.None));
        Assert.That(exception, Is.Not.Null);
        Assert.That(exception!.Id, Is.EqualTo(orgId));
        userStore.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Never);
        return Task.CompletedTask;
    }

    [Test]
    public Task Handle_WhenUserNotFound_ThrowsUserNotFoundException()
    {
        // Arrange
        var userId = Guid.NewGuid();
        var request = new AddUserToOrganizationCommand
        {
            OrganizationId = _organization.Id,
            UserId = _user.Id
        };
        
        var userStore = new Mock<IUserRepository>();
        var organizationStore = new Mock<IOrganizationRepository>();

        userStore.Setup(x => x.GetAsync(request.UserId)).ReturnsAsync(() => null);
        organizationStore.Setup(x => x.GetAsync(request.OrganizationId)).ReturnsAsync(_organization);

        var handler =
            new AddUserToOrganizationCommandHandler(userStore.Object, organizationStore.Object, _loggerMock.Object);

        // Act & Assert
        var exception = Assert.ThrowsAsync<UserNotFoundException>(() =>
            handler.Handle(request, CancellationToken.None));
        Assert.That(exception, Is.Not.Null);
        Assert.That(exception!.Id, Is.EqualTo(userId));
        userStore.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Never);
        return Task.CompletedTask;
    }
}