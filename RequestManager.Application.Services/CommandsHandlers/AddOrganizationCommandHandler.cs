using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using RequestManager.Application.Abstractions.Commands;
using Shared.Models;


namespace RequestManager.Application.Services.CommandsHandlers;

/// <summary>
/// Обработчик команды для отправки сообщения (добавление организации) в обменник с типом Fanout 
/// </summary>
/// <param name="producer">Интерфейс генерирующий отправку сообщения по шине</param>
public class AddOrganizationCommandHandler(
    IPublishEndpoint publishEndpoint,
    ILogger<AddOrganizationCommandHandler> logger)
    : IRequestHandler<AddOrganizationCommand>
{
    /// <summary>
    /// Метод обработчик команды по добавлению пользователя
    /// </summary>
    /// <param name="request">Команда для добавления пользователя</param>
    /// <param name="cancellationToken">Токен для отмены операции</param>
    /// <returns>Task</returns>
    public Task Handle(AddOrganizationCommand request, CancellationToken cancellationToken)
    {
        //Логируем отправку
        logger.LogInformation("Отправка данных организации по шине: {Name} ",
            request.Name);

        //Публикуем сообщение
        publishEndpoint.Publish(new OrganizationCreated{Name = request.Name});
        
        //возвращаем удачный результат задачи
        return Task.CompletedTask;
    }
}