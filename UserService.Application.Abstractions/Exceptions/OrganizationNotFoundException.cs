namespace UserService.Application.Abstractions.Exceptions;

/// <summary>
/// Исключение при отсутствии организации
/// </summary>
/// <param name="id">Идентификатор организации</param>
public class OrganizationNotFoundException(Guid id) : Exception($"Organization with id {id} not found")
{
    /// <summary>
    /// Идентификатор организации
    /// </summary>
    public Guid Id = id;
}
