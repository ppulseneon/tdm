using MediatR;
using UserService.Domain.OrganizationAggregate;

namespace UserService.Application.Abstractions.Queries;

/// <summary>
/// Запрос для получения организаций
/// </summary>
public class GetOrganizationsQuery : IRequest<ICollection<Organization>>
{
    /// <summary>
    /// Количество записей которые нужно пропустить
    /// </summary>
    public required int Skip { get; init; } 

    /// <summary>
    /// Количество записей которые нужно взять
    /// </summary>
    public required int Take { get; init; } 
}