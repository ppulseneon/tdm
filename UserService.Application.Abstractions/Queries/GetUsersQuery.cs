using MediatR;
using UserService.Domain.UserAggregate;

namespace UserService.Application.Abstractions.Queries;

/// <summary>
/// Запрос на получение пользователей у указанной организации
/// </summary>
public class GetUsersQuery : IRequest<ICollection<User>>
{
    /// <summary>
    /// Id организации
    /// </summary>
    public required Guid? OrganizationId { get; init; }

    /// <summary>
    /// Количество записей которые нужно пропустить
    /// </summary>
    public required int Skip { get; init; }

    /// <summary>
    /// Количество записей которые нужно взять
    /// </summary>
    public required int Take { get; init; }
}