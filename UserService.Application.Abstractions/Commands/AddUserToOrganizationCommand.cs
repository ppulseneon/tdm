using MediatR;

namespace UserService.Application.Abstractions.Commands;

/// <summary>
/// Команда на связывание пользователя с организацией 
/// </summary>
public class AddUserToOrganizationCommand : IRequest
{
    /// <summary>
    /// Id пользователя
    /// </summary>
    public required Guid UserId { get; init; } 
    
    /// <summary>
    /// Id организации
    /// </summary>
    public required Guid OrganizationId { get; init; }
}