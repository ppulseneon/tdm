using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RequestManager.Application.Abstractions.Commands;
using RequestManager.Presentation.Contracts.Organization;

namespace RequestManager.Presentation.Controllers;

/// <summary>
/// Контроллер управления организациями
/// </summary>
/// <param name="mediator">Интерфейс посредника</param>
[ApiController]
[Route("api/[controller]")]
public class OrganizationController(ISender mediator, IValidator<AddOrganizationInputModel> validatorOrganizationInputModel) : ControllerBase
{
    /// <summary>
    /// Метод для отравки команды на добавление организации (fanout). 
    /// </summary>
    /// <param name="model">Входная мадель для добавления организации</param>
    /// <param name="cancellationToken">Токен отмены</param>
    ///<response code="200">Запрос выполнен успешно</response>
    [HttpPost]
    public async Task<IActionResult> AddOrganization(AddOrganizationInputModel model,
        CancellationToken cancellationToken = default)
    {
        //получаем результат валидации
        var validationResult = await validatorOrganizationInputModel.ValidateAsync(model, cancellationToken);

        //проверка валидности модели
        if (!validationResult.IsValid)
        {
            //Отправляем код 400
            return BadRequest(new
            {
                Error = validationResult.Errors.Select(x => $"{x.PropertyName}: {x.ErrorMessage}").ToArray()
            });
        }
        
        //Отправляем команду на добавление организации
        await mediator.Send(new AddOrganizationCommand
        {
            Name = model.Name
        }, cancellationToken);

        //Отправляем код 200
        return Ok();
    }
}