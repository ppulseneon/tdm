using System.Reflection;

namespace RequestManager.Presentation;
/// <summary>
/// Класс содержит ссылку на сборку Presentation
/// </summary>
public static class AssemblyReference
{
    /// <summary>
    /// Ссылка на сборку
    /// </summary>
    public static readonly Assembly Assembly = typeof(AssemblyReference).Assembly;
}