namespace RequestManager.Presentation.Contracts.Organization;

/// <summary>
/// Входная модель добавления организации для контроллера
/// </summary>
public record AddOrganizationInputModel(string Name);