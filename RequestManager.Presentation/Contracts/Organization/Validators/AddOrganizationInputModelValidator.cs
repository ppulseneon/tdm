using FluentValidation;

namespace RequestManager.Presentation.Contracts.Organization.Validators;

/// <summary>
/// Валидатор для AddOrganizationInputModel
/// </summary>
public class AddOrganizationInputModelValidator:AbstractValidator<AddOrganizationInputModel>
{
    /// <summary>
    /// Правила валидации
    /// </summary>
    public AddOrganizationInputModelValidator()
    {
        RuleFor(e => e.Name)
            .NotEmpty().WithMessage("Название является обязательным полем.")
            .MinimumLength(3).WithMessage("Название должно составлять как минимум 3 буквы.")
            .MaximumLength(50).WithMessage("Название не может превышать 50 символов.");
    }
}