namespace RequestManager.Presentation.Contracts.User;

/// <summary>
/// Входная модель добавления пользователя для контроллера
/// </summary>
/// <param name="FirstName">Имя</param>
/// <param name="LastName">Фамилия</param>
/// <param name="Patronymic">Отчество</param>
/// <param name="PhoneNumber">Номер телефона</param>
/// <param name="Email">Электронная почта</param>
public record AddUserInputModel(string FirstName, string LastName, string? Patronymic, string PhoneNumber, string Email);