using FluentValidation;

namespace RequestManager.Presentation.Contracts.User.Validators;
/// <summary>
/// 
/// </summary>
public class AddUserInputModelValidator: AbstractValidator<AddUserInputModel>
{
    public AddUserInputModelValidator()
    {
        RuleFor(x => x.FirstName)
            .NotEmpty().WithMessage("Имя является обязательным полем.")
            .MinimumLength(3).WithMessage("Имя должно составлять как минимум 3 буквы.")
            .MaximumLength(50).WithMessage("Имя не может превышать 50 символов.");

        RuleFor(x => x.LastName)
            .NotEmpty().WithMessage("Фамилия является обязательным полем.")
            .MinimumLength(3).WithMessage("Фамилия должна составлять как минимум 3 буквы.")
            .MaximumLength(50).WithMessage("Фамилия не может превышать 50 символов.");

        RuleFor(x => x.Patronymic)
            .MinimumLength(3).WithMessage("Отчество должно составлять как минимум 3 буквы.")
            .MaximumLength(50).WithMessage("Отчество не может превышать 50 символов.");

        RuleFor(x => x.PhoneNumber)
            .NotEmpty().WithMessage("Номер телефона является обязательным полем.")
            .Matches(@"^\+\d{11}$").WithMessage("Номер телефона должен быть в формате +XXXXXXXXXXX, где X - цифра.");

        RuleFor(x => x.Email)
            .NotEmpty().WithMessage("Email является обязательным полем.")
            .EmailAddress().WithMessage("Неверный формат Email.");
    }
}