using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using Shared.Models;
using UserService.Application.Abstractions.Commands;

namespace UserService.Infrastructure.Bus.Consumers;

/// <summary>
/// Класс подписчик на очередь сообщений добавляющий организацию ExchangeType - fanout
/// </summary>
/// <param name="mediator">Интерфейс посредника</param>
/// <param name="logger">Интерфейс логгера</param>
public class OrganizationCreatedConsumer(IMediator mediator, ILogger<OrganizationCreatedConsumer> logger): IConsumer<OrganizationCreated>
{
    /// <summary>
    /// Обработка полученного сообщения
    /// </summary>
    /// <param name="context">Контекст сообщения с информацией о организации</param>
    public Task Consume(ConsumeContext<OrganizationCreated> context)
    {
        //логирование
        logger.LogInformation("Принята информация по шине: {Name}", context.Message.Name);
        
        //отправляем команду на добавление организации
        return mediator.Send(new AddOrganizationCommand
            {
                Name = context.Message.Name
            }
        );
    }
}