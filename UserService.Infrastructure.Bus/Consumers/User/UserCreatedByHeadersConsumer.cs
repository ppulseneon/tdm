using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using Shared.Models;
using UserService.Application.Abstractions.Commands;

namespace UserService.Infrastructure.Bus.Consumers.User;

/// <summary>
/// Класс подписчик на очередь сообщений (добавление пользователя). ExchangeType - Headers 
/// </summary>
/// <param name="mediator">Интерфейс посредника</param>
/// <param name="logger">Интерфейс логгера</param>
public class UserCreatedByHeadersConsumer(IMediator mediator, ILogger<UserCreatedByHeadersConsumer> logger) : IConsumer<UserCreatedByHeaders>
{
    /// <summary>
    /// Метод обработки полученного сообщения
    /// </summary>
    /// <param name="context">Контекст сообщения</param>
    public Task Consume(ConsumeContext<UserCreatedByHeaders> context)
    {
        //логирование
        logger.LogInformation("(Headers) Принята информация по шине: {LastName} {FirstName}", context.Message.LastName,
            context.Message.FirstName);
          
        //отправляем команду на добавление пользователя
        return mediator.Send(new AddUserCommand
        {
            FirstName = context.Message.FirstName,
            LastName = context.Message.LastName,
            Patronymic = context.Message.Patronymic,
            Email = context.Message.Email,
            PhoneNumber = context.Message.PhoneNumber
        });
    }
}