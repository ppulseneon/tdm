using MediatR;

namespace RequestManager.Application.Abstractions.Commands;
/// <summary>
/// Команда отправки сообщения (добавление организации) в обменник с типом Fanout
/// </summary>
public class AddOrganizationCommand:IRequest
{
    /// <summary>
    /// Название организации 
    /// </summary>
    public required string Name { get; init; } 
}