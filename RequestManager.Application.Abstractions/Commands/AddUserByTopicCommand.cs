using MediatR;

namespace RequestManager.Application.Abstractions.Commands;

/// <summary>
/// Команда отправки сообщения (добавление пользователя) в обменник с типом Topic
/// </summary>
/// <param name="firstName">Имя</param>
/// <param name="lastName">Фамилия</param>
/// <param name="phoneNumber">Номер телефона</param>
/// <param name="email">Элеткронная почта</param>
/// <param name="patronymic">Отчество</param>
public class AddUserByTopicCommand: IRequest
{
    /// <summary>
    /// Имя
    /// </summary>
    public required string FirstName { get; init; }

    /// <summary>
    /// Фамилия
    /// </summary>
    public required string LastName { get; init; }

    /// <summary>
    /// Отчество
    /// </summary>
    public string? Patronymic { get; init; } 

    /// <summary>
    /// Номер телефона
    /// </summary>
    public required string PhoneNumber { get; init; } 

    /// <summary>
    /// Почта
    /// </summary>
    public required string Email { get; init; } 
}