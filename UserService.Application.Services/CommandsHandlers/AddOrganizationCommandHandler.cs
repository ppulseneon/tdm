using MediatR;
using UserService.Application.Abstractions.Commands;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.OrganizationAggregate;

namespace UserService.Application.Services.CommandsHandlers;
/// <summary>
/// Обработчик команды добавления организации
/// </summary>
/// <param name="repository">Интерфейс репозитория организации</param>
public class AddOrganizationCommandHandler(IOrganizationRepository repository):IRequestHandler<AddOrganizationCommand>
{
    public Task Handle(AddOrganizationCommand request, CancellationToken cancellationToken)
    {
        //Создание организации
        Organization org = Organization.Create(Guid.NewGuid(), request.Name);
        
        //Добавление организации в бд
        return repository.AddAsync(org);
    }
}


