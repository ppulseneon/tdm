using MediatR;
using UserService.Application.Abstractions.Queries;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.OrganizationAggregate;

namespace UserService.Application.Services.QueriesHandlers;

/// <summary>
/// Обработчик запроса для получения организаций
/// </summary>
/// <param name="organizationStore">Интерфейс репозитория организации</param>
public class GetOrganizationsQueryHandler(IOrganizationRepository organizationStore) : IRequestHandler<GetOrganizationsQuery, ICollection<Organization>>
{
    public Task<ICollection<Organization>> Handle(GetOrganizationsQuery request, CancellationToken cancellationToken)
    {
        //Поиск организаций и возвращение коллекции 
        return organizationStore.FindAsync(request.Skip, request.Take);
    }
}