using MediatR;
using UserService.Application.Abstractions.Queries;
using UserService.Application.Abstractions.Exceptions;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.UserAggregate;

namespace UserService.Application.Services.QueriesHandlers;

/// <summary>
/// Обработчик запроа для получения пользователей из организации
/// </summary>
/// <param name="userStore">Интерфейс репозитория пользователя</param>
/// <param name="organizationStore">Интерфейс репозитория организации</param>
public class GetUsersQueryHandler(IUserRepository userStore, IOrganizationRepository organizationStore)
    : IRequestHandler<GetUsersQuery, ICollection<User>>
{
    public async Task<ICollection<User>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
    {
        //Проверка параметров запроса 
        if (request.Skip < 0 && request.Take <= 0)
        {
            //Если параметры неккоректные выбрасываем исключение
            throw new Exception("Неккоректные параметры запроса");
        }

        //Проверка наличия организации
        if (request.OrganizationId.HasValue)
        {
            //Проверка существования организации
            var organization = await organizationStore.GetAsync(request.OrganizationId.Value);
            
            //Если организация не существует выбрасываем исключение
            if (organization == null) throw new OrganizationNotFoundException(request.OrganizationId.Value);
        }

        //Поиск пользователей выбранной организации и возвращение коллекции
        return await userStore.FindAsync(request.OrganizationId, request.Skip, request.Take);;
    }
}