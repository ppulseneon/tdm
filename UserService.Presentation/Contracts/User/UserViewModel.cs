namespace UserService.Presentation.Contracts.User;

/// <summary>
/// Выводная модель пользователя для контроллера
/// </summary>
/// <param name="Id">Идентификатор пользователя</param>
/// <param name="FirstName">Имя</param>
/// <param name="LastName">Фамилия</param>
/// <param name="Patronymic">Отчество</param>
/// <param name="PhoneNumber">Номер телефона</param>
/// <param name="Email">Электронная почта</param>
public record UserViewModel(Guid Id, string FirstName, string LastName, string? Patronymic, string PhoneNumber, string Email);