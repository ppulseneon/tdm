namespace UserService.Presentation.Contracts.User;

/// <summary>
/// Входная модель добавления пользователя в организацию для контроллера
/// </summary>
/// <param name="Userid">Идентификатор пользователя</param>
/// <param name="OrganizationId">Идентификатор организации</param>
public record AddUserToOrganizationInputModel(Guid? Userid, Guid? OrganizationId);