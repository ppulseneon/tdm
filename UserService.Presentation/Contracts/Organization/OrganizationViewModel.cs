namespace UserService.Presentation.Contracts.Organization;

/// <summary>
/// Выводная модель организации для контроллера
/// </summary>
/// <param name="Id">Идентификатор организации</param>
/// <param name="Name">Название организации</param>
public record  OrganizationViewModel(Guid Id, string Name);