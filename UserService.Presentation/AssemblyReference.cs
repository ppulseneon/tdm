using System.Reflection;

namespace UserService.Presentation;

/// <summary>
/// Класс содержит ссылку на сборку Presentation
/// </summary>
public static class AssemblyReference
{
    /// <summary>
    /// Cсылка на сборку
    /// </summary>
    public static readonly Assembly Assembly = typeof(AssemblyReference).Assembly;
}