﻿using Serilog.Sinks.Elasticsearch;
using System.Reflection;

namespace UserService.App.Settings;

/// <summary>
/// Статический класс для создания настроек подключения ElasticSearch
/// </summary>
public static class ElasticSearchSettings
{
    /// <summary>
    /// Метод получения настроек подключения Elasticsearch 
    /// </summary>
    public static ElasticsearchSinkOptions GetElasticsearchSinkOptions(IConfiguration configuration)
    {
        // Получаем строку подключения
        var elasticConnectionString = configuration.GetConnectionString("ElasticSearch") ?? throw new Exception("ConnectionStrings:ElasticSearch");

        // Конфигурируем настройки
        var elasticSettings = new ElasticsearchSinkOptions(new Uri(elasticConnectionString))
        {
            AutoRegisterTemplate = true,
            IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name.ToLower().Replace('.', '-')}-{DateTime.Now:yyyy.MM.dd}",
            NumberOfReplicas = 2,
            NumberOfShards = 2,
        };

        return elasticSettings;
    }
}