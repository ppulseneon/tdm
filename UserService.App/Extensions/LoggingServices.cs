using Serilog;
using UserService.App.Settings;

namespace UserService.App.Extensions;

/// <summary>
/// Статический класс для регистрации сервиса Serilog в контейнере DI 
/// </summary>
public static class LoggingServices
{
    /// <summary>
    /// Метод регистрирует сервис Serilog в контейнере DI 
    /// </summary>
    /// <param name="builder">Построитель веб-приложений и сервисов.</param>
    public static void AddLoggingServices(this WebApplicationBuilder builder)
    {
        // Получаем конфигурацию проекта
        var configuration = builder.Configuration;

        // Получаем строку подключения к ElasticSearch
        var elasticSearchOptions = ElasticSearchSettings.GetElasticsearchSinkOptions(configuration);

        Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(configuration)
            .WriteTo.Elasticsearch(elasticSearchOptions)
            .CreateLogger();

        builder.Host.UseSerilog();
    }
}