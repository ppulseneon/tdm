using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using RequestManager.Application.Abstractions.Commands;
using RequestManager.Presentation.Contracts.User;
using RequestManager.Presentation.Contracts.User.Validators;
using RequestManager.Presentation.Controllers;

namespace RequestManager.Tests;

[TestFixture]
public class UserControllerTests
{
    private UserController _controller = null!;
    private Mock<IMediator> _mediatorMock = null!;

    [SetUp]
    public void SetUp()
    {
        _mediatorMock = new Mock<IMediator>();
        
        _mediatorMock.Setup(x => x.Send(It.IsAny<AddUserCommand>(), CancellationToken.None))
            .Returns(Task.CompletedTask);
        
        var validator = new AddUserInputModelValidator();

        _controller = new UserController(_mediatorMock.Object, validator);
    }

    [Test]
    public async Task AddUser_WithValidModel_ReturnsOk()
    {
        // Arrange
        var model = new AddUserInputModel(
            Email: "zvg@yandex.com",
            FirstName: "Максим",
            LastName: "Завгородний",
            Patronymic: "Валерьевич",
            PhoneNumber: "+79586745866"
        );

        // Act
        var result = await _controller.AddUser(model, CancellationToken.None) as OkResult;
        
        // Assert
        Assert.That(result, Is.Not.Null);
        Assert.That(result!.StatusCode, Is.EqualTo(200));
    }
    
    [Test]
    [TestCase("Ма", "Завгородний", "+79586745866", "zvg@yandex.com", "Валерьевич")]
    [TestCase("Максим", "Завгородний", "745866", "zvg@yandex.com", "Валерьевич")]
    [TestCase("Максим", "Завгородний", "+79586745866", "zvg", "Валерьевич")]
    [TestCase("Максим", "За", "+79586745866", "zvg@yandex.com", "Валерьевич")]
    [TestCase("Максим", "Завгородний", "+79586745866", "zvg@yandex.com", "Ва")]
    [TestCase(null, "Завгородний", null, "zvg@yandex.com", "Ва")]
    [TestCase("Максим", "Завгородний", "+79586745866", "zvg@yandex.com", "Ва")]
    [TestCase("Максим", "", "", "zvg@yandex", "Валерьевич")]
    public async Task AddUser_WithInvalidModel_ReturnsBadRequest(string firstName, string lastName, string phone, string email,
        string patronymic)
    {
        // Arrange
        var model = new AddUserInputModel(
            Email: email,
            FirstName: firstName,
            LastName: lastName,
            Patronymic: patronymic,
            PhoneNumber: phone);
        
        // Act
        var result = await _controller.AddUser(model, CancellationToken.None) as BadRequestObjectResult;

        // Assert
        Assert.That(result, Is.Not.Null);
        Assert.That(result!.StatusCode, Is.EqualTo(400));
    }
}