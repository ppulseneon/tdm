using UserService.Domain.UserAggregate;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Mappers.ModelMappers;

/// <summary>
/// Класс содержит метод преобразования агрегата в модель организации
/// </summary>
public class UserModelMapper
{
    /// <summary>
    /// Метод преобразует агрегат пользователя в модель пользователя
    /// </summary>
    /// <param name="userAggregate">Агрегат пользователя</param>
    /// <param name="userModel">Модель пользователя</param>
    /// <returns></returns>
    public static UserModel Map(User userAggregate, UserModel userModel)
    {
        //Присваивание имени 
        userModel.FirstName = userAggregate.FirstName;
        
        //Присваивание фамилии
        userModel.LastName = userAggregate.LastName;
        
        //Присваивание отчества
        userModel.Patronymic = userAggregate.Patronymic;
        
        //Присваивание почты
        userModel.Email = userAggregate.Email;
        
        //Присваивание телефона 
        userModel.PhoneNumber = userAggregate.PhoneNumber;

        //Проверка на наличие организации у агрегата пользователя
        if (userAggregate.OrganizationId.HasValue)
        {
            //Агрегат имеет организацию, присваиваем значение идентификатора организации
            userModel.OrganizationId = userAggregate.OrganizationId;
        }

        return userModel;
    }
}