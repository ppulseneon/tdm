using UserService.Domain.OrganizationAggregate;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Mappers.ModelMappers;
/// <summary>
/// Класс содержит метод преобразования агрегата в модель организации
/// </summary>
public static class OrganizationModelMapper
{
    /// <summary>
    /// Метод преобразует агрегат организации в модель организации
    /// </summary>
    /// <param name="organizationAggregate">Агрегат организации</param>
    /// <param name="organizationModel">Модель организации</param>
    /// <returns></returns>
    public static OrganizationModel Map(Organization organizationAggregate, OrganizationModel organizationModel)
    {
        //Присваивание названия организации
        organizationModel.Name = organizationAggregate.Name;
        
        //Возвращение модели
        return organizationModel;
    }
}
