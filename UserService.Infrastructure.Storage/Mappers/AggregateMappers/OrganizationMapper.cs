using UserService.Domain.OrganizationAggregate;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Mappers.AggregateMappers;

/// <summary>
/// Класс содержит метод преобразования модели организации в агрегат
/// </summary>
public static class OrganizationMapper
{
    /// <summary>
    /// Метод преобразует модель организации в агрегат
    /// </summary>
    /// <param name="model">Модель организации</param>
    /// <returns>Агрегат организации</returns>
    public static Organization Map(OrganizationModel model)
    {
        //Создание агрегата
        var org = Organization.Create(
            model.Id,
            model.Name
        );
        
        //Возвращение агрегата
        return org;
    }
}