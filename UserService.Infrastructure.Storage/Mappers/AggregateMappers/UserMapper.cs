using UserService.Domain.OrganizationAggregate;
using UserService.Domain.UserAggregate;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Mappers.AggregateMappers;

/// <summary>
/// Класс содержит метод преобразования модели пользователя в агрегат
/// </summary>
public static class UserMapper
{
    /// <summary>
    /// Метод преобразует модель пользователя в агрегат
    /// </summary>
    /// <param name="model">Модель пользователя</param>
    /// <returns>Агрегат пользователя</returns>
    public static User Map(UserModel model)
    {
        //Создание агрегата
        var user = User.Create(
            model.Id,
            model.FirstName,
            model.LastName,
            model.Patronymic,
            model.Email,
            model.PhoneNumber,
            model.Organization?.Id);

        //Возвращение агрегата
        return user;
    }
}