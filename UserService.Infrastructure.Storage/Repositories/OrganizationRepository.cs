using Microsoft.EntityFrameworkCore;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.OrganizationAggregate;
using UserService.Infrastructure.Storage.Context;
using UserService.Infrastructure.Storage.Mappers.AggregateMappers;
using UserService.Infrastructure.Storage.Mappers.ModelMappers;
using UserService.Infrastructure.Storage.Models;


namespace UserService.Infrastructure.Storage.Repositories;

/// <summary>
/// Реализация интерфейса IOrganizationRepository
/// </summary>
public class OrganizationRepository:IOrganizationRepository
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    private readonly ApplicationDbContext _context;
    
    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    public OrganizationRepository(ApplicationDbContext context)
    {
        _context = context;
    }
    
    /// <summary>
    /// Получение организации по идентификатору асинхронно
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Агрегат организации</returns>
    public async Task<Organization?> GetAsync(Guid id)
    {
        //Получение организации по id
        var organizationModel = await _context.Organizations
            .AsNoTracking()
            .FirstOrDefaultAsync(u => u.Id == id);
        
        //В случае отсутствия организации возвращаем null, иначе возвращаем агрегат организации
        return organizationModel == null ? null : OrganizationMapper.Map(organizationModel);
    }

    /// <summary>
    /// Обновление организации асинхронно
    /// </summary>
    /// <param name="organization">Агрегат организации</param>
    public async Task UpdateAsync(Organization organization)
    {
        //Получение организации по id
        var organizationModel = await _context.Organizations.FirstOrDefaultAsync(u => u.Id == organization.Id);
        
        //Если организация не найдена выходим из метода
        if (organizationModel == null) return;
        
        //Преобразование агрегата организации в модель
        var org = OrganizationModelMapper.Map(organization, organizationModel);
        
        //Обновление организации в бд 
        _context.Organizations.Update(org);
        
        //Сохранение изменений
        await _context.SaveChangesAsync();
        
    }
    
    /// <summary>
    /// Добавление организации асинхронно
    /// </summary>
    /// <param name="organization">Агрегат организации</param>
    public async Task AddAsync(Organization organization)
    {
        //Cоздание новой модели организации
        var newOrganizationModel = new OrganizationModel{ Id = organization.Id };
        
        //Преобразование агрегата организации в модель
        var organizationModel = OrganizationModelMapper.Map(organization, newOrganizationModel);
        
        //Добавление организации в бд
        _context.Organizations.Add(organizationModel);
        
        //Cохранение изменений
        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Поиск организаций по параметрам асинхронно
    /// </summary>
    /// <param name="skip">Количество записей которые нужно пропустить</param>
    /// <param name="take">Количество записей которые нужно получить</param>
    /// <returns>Список агрегатов организаций</returns>
    public async Task<ICollection<Organization>> FindAsync(int skip, int take)
    {
        //Получние списка организаций
        var organizations = await _context.Organizations
            .OrderBy(o => o.Id)
            .Skip(skip)
            .Take(take)
            .ToListAsync();
        
        //Преобразование списка моделей в cписок агрегатов организаций
        return organizations
            .Select(o => OrganizationMapper.Map(o))
            .ToList();
    }
}